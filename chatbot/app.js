function getDateAsString(date) {
  var today = date || new Date()
  // var date = today.getDate()+'-'+(today.getMonth()+1)+'-'+today.getFullYear()
  var time = today.getHours() + ':' + today.getMinutes()
  return time
}

function parseCommand(text) {
  // check if 1st char is a / if not log to console and return [null, null] (also remove the / once the check is done)
  if (text.charAt(0) === '/') {
    text = text.substring(1)
  } else {
    return [null, null]
  }

  // parse the str (separation is the space char)
  let parsed = text.split(' ')
  let cmdName = parsed.shift()
  let args = parsed // .shift removed the first entry in the array

  return [cmdName, args]
}

var app = new Vue({
  el: '#app',
  data: {
    helpIsOpen: false,
    messagesBacklog: [
      {
        author: '#eroy-the-bot',
        date: getDateAsString(),
        text: 'Hello human, what do you want me to uselessly automate today?',
        userOriginated: false,
      },
    ],
    chatMessage: '',
    search: '',
  },
  computed: {
    messages: function () {
      return this.messagesBacklog.filter((el) => {
        return (
          el.author.includes(this.search) ||
          el.date.includes(this.search) ||
          el.text.includes(this.search)
        )
      })
    },
  },
  methods: {
    handleMessage: function () {
      let [cmd, args] = parseCommand(this.chatMessage)
      if (!cmd) {
        this.messagesBacklog.push({
          author: 'You',
          date: getDateAsString(),
          text: this.chatMessage,
          userOriginated: true,
        })
      } else {
        if (cmd === 'clear') {
          this.messagesBacklog = []
        } else if (cmd === 'lorem-ipsum') {
          this.messagesBacklog.push({
            author: 'You',
            date: getDateAsString(),
            text: this.chatMessage,
            userOriginated: true,
          })

          let n = args[0] ?? 1
          for (let i = 0; i < n; i++) {
            this.messagesBacklog.push({
              author: '#eroy-the-bot',
              date: getDateAsString(),
              text:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut facilisis felis turpis, sed aliquet odio eleifend sed. Vivamus maximus consequat rhoncus. Cras molestie varius malesuada. Etiam ultrices convallis sollicitudin. Praesent elementum turpis erat, eget posuere lorem pharetra sit amet. Donec tristique mollis aliquam. Pellentesque sit amet dignissim elit. Aenean condimentum scelerisque ante, quis tincidunt metus varius at. Suspendisse felis lorem, cursus nec finibus nec, maximus ac purus.',
              userOriginated: false,  
            })
          }
        }
        else {
          this.messagesBacklog.push({
            author: '#eroy-the-bot',
            date: getDateAsString(),
            text: "Failed to parse command " + this.chatMessage,
            userOriginated: false,
          })
        }
      }
      this.chatMessage = ''
    },
    toggleHelp: function() {
      this.helpIsOpen = !this.helpIsOpen
    }
  },
})
